package com.example.adapterexample;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class DropDown extends AppCompatActivity {



    // 1. make a data source
    // 2. Make a variable for you rUI component (LIST VIEW)
    // 3. Make the adapter
    // 4. Connect the adapter to the Listview

    // STEP 1
//    String countryNames[] = {"Canada", "USA","Mexico", "Belize", "Guatemala", "Honduras"};
//    String animals[] = {"dog", "cat", "bird"};


    String countryNames[] = {"Canada", "USA","Mexico", "Belize", "Guatemala", "Honduras",
            "El Salvador", "Nicaragua", "Costa Rica", "Panama", "Cuba", "Bahamas",
            "Dominican Republic", "Haiti", "Puerto Rico", "Anguilla", "Antigua and Barbuda",
            "Saint Kitts and Nevis", "Guadeloupe", "Dominica", "Martinique"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drop_down);

        // STEP 2
        ListView lv = (ListView) findViewById(R.id.countries_list_view);


        // STEP 3
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                countryNames);

        // STEP 4
        lv.setAdapter(adapter);

        // STEP 5 - Handle clicks from your list view
        lv.setClickable(true);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Toast t = Toast.makeText(getApplicationContext(), "hello!", Toast.LENGTH_SHORT);
                t.show();


            }
        });



    }
}
